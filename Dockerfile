FROM python:slim

# Install requirements
ADD requirements.txt /app/requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install -r /app/requirements.txt

# Set working dir
ADD main.py /app/main.py
WORKDIR /app

# Pass env vars
ENV PYTHONUNBUFFERED 1

# Run container rootless
RUN groupadd -g 1000 pyuser
RUN useradd --create-home --no-log-init -u 1000 -g 1000 pyuser
RUN chown pyuser:pyuser  /app
USER pyuser

# Run script
CMD ["python", "main.py"]