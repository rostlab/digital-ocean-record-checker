
# Containerized Digital Ocean record updater

A simple script that uses a DigitalOcean access token to login and update the A record of any domain you enter to the WAN ip of the executing environment.

## How to use
#### Use pre-build image

Pull a specific version if you know what you are doing or just pull latest : 

~~~
docker pull registry.gitlab.com/rostlab/applications/digital-ocean-record-checker:0.1.0

docker pull registry.gitlab.com/rostlab/applications/digital-ocean-record-checker:latest
~~~
### OR
#### Build your own image
clone this repository:
~~~
git clone git@gitlab.com:rostlab/digital-ocean-record-checker.git
~~~

Define where the container will be hosted, like dockerhub

Build the container where you replace example user with your username and project with the name of the project:
~~~
docker login -u "exampleuser" -p {acces_token}
docker build -t exampleuser/project .
docker push exampleuser/project
~~~
after pushing it, the image can be used elsewhere

~~~
docker pull exampleuser/project:latest
~~~

## Run container in on-prem infrastructure to allow the WAN ip to match actual value
Create a container and set environmental variables:
The websites need to be set as a single line seperated by comma's.<br>
<br>

| Variable              | Value                                         | expected type |
|-----------------------|-----------------------------------------------|---------------|
| DIGITAL_OCEAN_API_KEY | dop_v1_123456789qwertyuiop                    | string        |
| WEBSITES              | subdomain.google.com,*.apple.com,@.amazon.com | string        |
| ALLOW_CREATE_RECORD   | Yes / True / 1                                | string        |
| ALLOW_DELETE_RECORD   | Yes / True / 1                                | string        |
Not setting any value for ALLOW_CREATE_RECORD will only update existing DNS records. Setting this to True will create the record if it is not found
Not setting any value for ALLOW_DELETE_RECORD will ignore all A records not supplied by WEBSITES. Setting this to true will remove all A records not supplied by WEBSITES.
<br>
<br>

Schedule in task scheduler if repeat occurances are neccessary

# Known issues

Module will raise the following error if the domain name cannot be found in Digital Ocean, but error cannot be caught.
```console
$ digitalocean.NotFoundError
```
