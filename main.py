import os
from requests import get
import digitalocean


def update_record(do, domain, subdomain, wan_ip):
    records = do.get_records()
    for record in records:
        if record.type == 'A' and record.name == subdomain:
            if record.ttl != 3600:
                record.ttl = 3600
                record.save()
            if record.data != wan_ip:
                record.data = wan_ip
                record.save()
                return True
            else:
                return False


def create_record(do, subdomain, wan_ip):
    do.create_new_domain_record(type="A",
                                name=subdomain,
                                data=wan_ip,
                                ttl=60)


def create_subdomain_var(website):
    subdomain_list = website.replace(" ", "").split(".")[:-2]
    subdomain = ""
    if len(subdomain_list) > 1:
        for sd in subdomain_list:
            subdomain = subdomain + "{0}.".format(sd)
        subdomain = subdomain[:-1]  # remove trailing period
    else:
        subdomain = subdomain_list[0]
    return subdomain


def create_domain_var(website):
    domain_list = website.split(".")[-2:]
    return "{0}.{1}".format(domain_list[0], domain_list[1])


def delete_record(token, website_list):
    domain_list = []
    fqdn_records = []

    for website in website_list:
        domain_list.append(create_domain_var(website=website))

    for domain in list(set(domain_list)):
        do = digitalocean.Domain(token=token, name=domain)
        record_list = do.get_records()
        for record in record_list:
            if record.type == "A":
                fqdn_records.append({"fqdn": "{0}.{1}".format(record.name, record.domain),
                                     "domain": domain,
                                     "do_record": record, })

    for item in fqdn_records:
        if item["fqdn"] not in website_list:
            do = digitalocean.Domain(token=token, name=item["domain"])
            do.delete_domain_record(id=item["do_record"].id)
            print("[COMPLETED] {0} | Record deleted. |".format(item["fqdn"]))


def verify_records(website, token, allow_create):
    # split FQDN into list and remove domain and tld
    subdomain = create_subdomain_var(website=website)
    domain = create_domain_var(website=website)

    # call DO to get domain based on domain value
    do = digitalocean.Domain(token=token, name=domain)
    do_domain_records = do.get_records()

    try:
        wan_ip = get('https://api.ipify.org').content.decode('utf8')
    except:
        wan_ip = get('https://ident.me').content.decode('utf8')

    subdomain_records = []
    fqdn_records = []

    for dr in do_domain_records:
        if dr.type == "A":
            subdomain_records.append(dr.name)
            fqdn_records.append("{0}.{1}".format(dr.name, dr.domain))

    if subdomain not in subdomain_records:
        # If record does not exist at Registrar and Allow_Create_record is true
        if str(allow_create).lower() in ["yes", "true", "1"]:
            create_record(do=do,
                          subdomain=subdomain,
                          wan_ip=wan_ip,
                          )
            return print("[COMPLETED] {0}.{1} | Record created. |".format(subdomain, domain))

    else:
        # If record exists, check if value matches. If not, update record
        action = update_record(do=do,
                      subdomain=subdomain,
                      domain=domain,
                      wan_ip=wan_ip)
        if action:
            return print("[COMPLETED] {0}.{1} | Record updated. |".format(subdomain, domain))
        else:
            return print("[COMPLETED] {0}.{1} | No changes necessary. |".format(subdomain, domain))


if __name__ == '__main__':
    # Validate if vars are available
    for env_var in ["WEBSITES", "DIGITAL_OCEAN_API_KEY"]:
        if os.getenv(env_var) is None:
            raise AttributeError("[ERROR] environment variable '%s' not found" % env_var)

    website_list = os.getenv("WEBSITES").split(sep=",")

    for website in set(os.getenv("WEBSITES").split(sep=",")):  # pass a string with comma's like "website1,website2,website3"
        verify_records(website=website,  # input website
                       token=os.getenv("DIGITAL_OCEAN_API_KEY"),  # input API token
                       allow_create=os.getenv("ALLOW_CREATE_RECORD"),
                       )  # set this to True allow record to be created if not found

    if str(os.getenv("ALLOW_DELETE_RECORD")).lower() in ["yes", "true", "1"]:
        delete_record(website_list=website_list,
                      token=os.getenv("DIGITAL_OCEAN_API_KEY"),
                      )
